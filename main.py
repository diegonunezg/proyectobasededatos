import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import psycopg2
from psycopg2 import Error

from crud_windows import *


class MainWindow(Gtk.Window):

    def __init__(self):
        super().__init__()

        self.connect_to_postgres()

        self.set_title("Interacciones entre especies")
        self.set_default_size(1500, 800)
        self.set_border_width(8)
        self.connect("destroy", self.cerrar_app)

        self.inicializar_interfaz()
        self.tabla_actual = None
        self.fila_actual_seleccionada = None


    def connect_to_postgres(self):

        try:
            self.connection = psycopg2.connect(
                user = "postgres",
                password = "delta733",
                host = "localhost",
                port = 5432,
                database = "relaciones_especies"
            )

            self.cursor = self.connection.cursor()

        except (Exception, Error) as error:
            print("Error en PostgreSQL", error)
            Gtk.main_quit()


    def inicializar_interfaz(self):

        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(False)
        self.grid.set_row_homogeneous(False)
        self.grid.set_column_spacing(8)
        self.grid.set_row_spacing(4)
        self.add(self.grid)

        self.create_toolbar()

        self.crear_vista_tablas()

        self.crear_botones_CRUD()


    def create_toolbar(self):

        toolbar = Gtk.Toolbar()
        toolbar.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.grid.attach(toolbar, 1, 0, 1, 1)

        registros_interacciones_button = Gtk.ToolButton()
        registros_interacciones_button.set_label("Registro de interacciones")
        registros_interacciones_button.connect("clicked", self.cambiar_tabla, "obtenerRegistrosInteraccion()")
        toolbar.insert(registros_interacciones_button, 0)

        especie_button = Gtk.ToolButton()
        especie_button.set_label("Especies")
        especie_button.connect("clicked", self.cambiar_tabla, "especie")
        toolbar.insert(especie_button, 1)

        tipo_interaccion_button = Gtk.ToolButton()
        tipo_interaccion_button.set_label("Tipos de interacciones")
        tipo_interaccion_button.connect("clicked", self.cambiar_tabla, "tipo_interaccion")
        toolbar.insert(tipo_interaccion_button, 2)

        ecosistema_button = Gtk.ToolButton()
        ecosistema_button.set_label("Ecosistemas")
        ecosistema_button.connect("clicked", self.cambiar_tabla, "ecosistema")
        toolbar.insert(ecosistema_button, 3)

        estudio_button = Gtk.ToolButton()
        estudio_button.set_label("Estudios")
        estudio_button.connect("clicked", self.cambiar_tabla, "estudio")
        toolbar.insert(estudio_button, 4)

        """
        interaccion_button = Gtk.ToolButton()
        interaccion_button.set_label("Interacciones")
        interaccion_button.connect("clicked", self.cambiar_tabla, "interaccion")
        toolbar.insert(interaccion_button, 5)
        """

        """
        registro_button = Gtk.ToolButton()
        registro_button.set_label("Registros")
        registro_button.connect("clicked", self.cambiar_tabla, "registro")
        toolbar.insert(registro_button, 6)
        """


    def crear_vista_tablas(self):

        self.scroll = Gtk.ScrolledWindow()
        self.scroll.set_hexpand(True)
        self.scroll.set_vexpand(True)
        self.grid.attach(self.scroll, 1, 1, 1, 2)

        self.treeview = Gtk.TreeView()
        self.scroll.add(self.treeview)
        self.modelo = Gtk.ListStore(str)
        self.treeview.set_model(self.modelo)
        self.crear_columnas()

        self.selection = self.treeview.get_selection()
        self.selection.connect("changed", self.on_tree_selection_changed)

    
    def on_tree_selection_changed(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter != None:
            self.fila_actual_seleccionada = model[treeiter][:]
        else:
            self.fila_actual_seleccionada = None

    
    def crear_botones_CRUD(self):
        label_CRUD = Gtk.Label(label=" Operaciones CRUD ")

        self.boton_create = Gtk.Button(label="Crear")
        self.boton_create.connect("clicked", self.abrir_ventana_create)
        self.boton_read = Gtk.Button(label="Leer")
        #self.boton_read.connect("clicked", )
        self.boton_update = Gtk.Button(label="Actualizar")
        self.boton_update.connect("clicked", self.abrir_ventana_update)
        self.boton_delete = Gtk.Button(label="Eliminar")
        #self.boton_delete.connect("clicked", )}
                
        self.CRUDbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.CRUDbox.set_spacing(2)
        self.CRUDbox.add(label_CRUD)
        self.CRUDbox.add(self.boton_create)
        self.CRUDbox.add(self.boton_read)
        self.CRUDbox.add(self.boton_update)
        self.CRUDbox.add(self.boton_delete)

        self.grid.attach(self.CRUDbox, 0, 1, 1, 1)


    def cerrar_app(self, widget):

        self.cursor.close()
        self.connection.close()
        Gtk.main_quit()
    
    
    def abrir_ventana_create(self, widget):

        ventana = CreateDialog()
        respuesta = ventana.run()

        if respuesta == Gtk.ResponseType.OK:
            accion = ventana.obtener_accion()

        ventana.destroy()

        if accion == "all":

            # Se preparan los datos de las especies
            cursor_especie = self.connection.cursor()
            cursor_especie.execute("SELECT * FROM especie")
            nombres_columnas_especie = [desc[0] for desc in cursor_especie.description]
            datos_especie = []
            for row in cursor_especie:
                datos_especie.append([str(x) for x in row])

            ventana = CreateWindow(nombres_columnas_especie, datos_especie)
            respuesta = ventana.run()

            ventana.destroy()

        elif accion == "current":
            pass


    def abrir_ventana_update(self, widget):

        cursor_update = self.connection.cursor()
        cursor_update.execute(f"SELECT * FROM {self.tabla_actual}")
        nombres_columnas = [desc[0] for desc in cursor_update.description]

        if self.tabla_actual == "obtenerRegistrosInteraccion()":
            data_global_columnas = []
            data_global_filas = []
            for tabla in ["especie", "tipo_interaccion", "ecosistema", "estudio"]:
                cursor_update.execute(f"SELECT * FROM {tabla}")
                data_global_columnas.append([desc[0] for desc in cursor_update.description])
                aux_list=[]
                for row in cursor_update:
                    aux_list.append([str(item) for item in row])
                data_global_filas.append(aux_list)
            
            ventana = UpdateWindow(self.fila_actual_seleccionada, nombres_columnas, self.tabla_actual, data_global_columnas, data_global_filas)

        else:
            ventana = UpdateWindow(self.fila_actual_seleccionada, nombres_columnas, self.tabla_actual)
        
        respuesta = ventana.run()

        if respuesta == Gtk.ResponseType.OK:
            cambios = ventana.get_cambios()
        
            if self.tabla_actual == "ecosistema":
                cursor_update.execute(f"CALL update_ecosistema({int(cambios[0])}, '{str(cambios[1])}', '{str(cambios[2])}')")
                self.connection.commit()
                self.cambiar_tabla(None, "ecosistema")

            elif self.tabla_actual == "especie":
                cursor_update.execute(f"CALL update_especie({int(cambios[0])}, '{str(cambios[1])}', '{str(cambios[2])}', '{str(cambios[3])}', '{str(cambios[4])}', '{str(cambios[5])}', '{str(cambios[6])}', '{str(cambios[7])}', '{str(cambios[8])}', '{str(cambios[9])}', '{str(cambios[10])}', '{str(cambios[11])}', '{str(cambios[12])}')")
                self.connection.commit()
                self.cambiar_tabla(None, "especie")

            elif self.tabla_actual == "estudio":
                cursor_update.execute(f"CALL update_estudio({int(cambios[0])}, '{str(cambios[1])}')")
                self.connection.commit()
                self.cambiar_tabla(None, "estudio")

            elif self.tabla_actual == "tipo_interaccion":
                cursor_update.execute(f"CALL update_tipo_interaccion({int(cambios[0])}, '{str(cambios[1])}', '{str(cambios[2])}')")
                self.connection.commit()
                self.cambiar_tabla(None, "tipo_interaccion")
            
            elif self.tabla_actual == "obtenerRegistrosInteraccion()":
                pass


        ventana.destroy()


    def crear_columnas(self, tabla=None):

        for columna in self.treeview.get_columns():
            self.treeview.remove_column(columna)
        
        if tabla is None:
            tabla = ["Selecciona una de los botones de arriba para ver la tabla asociada"]
        
        self.modelo = Gtk.ListStore(*([str]*len(tabla)))
        self.treeview.set_model(self.modelo)

        for i, nombre_columna in enumerate(tabla):
            celda = Gtk.CellRendererText()
            columna = Gtk.TreeViewColumn(nombre_columna, celda, text=i)
            self.treeview.append_column(columna)


    def cambiar_tabla(self, widget, query):

        self.tabla_actual = query

        self.treeview.get_model().clear()

        self.cursor.execute(f"SELECT * FROM {query};")

        column_names = [desc[0] for desc in self.cursor.description]

        self.crear_columnas(column_names)

        data_rows = []
        for row in self.cursor:
            data_rows.append(row)
        
        for item in data_rows:
            line = [str(x) for x in item]
            self.modelo.append(line)


def main():

    win = MainWindow()
    win.show_all()
    Gtk.main()


if __name__ == "__main__":
    main()