
/* INSTRUCCIONES DE CREACION DE LAS TABLAS */

CREATE TABLE ecosistema (
    idEcosistema SERIAL PRIMARY KEY NOT NULL, 
    nombre VARCHAR(50) NOT NULL, 
    descripción_ubicacion VARCHAR(300)
);

CREATE TABLE especie (
    idEspecie SERIAL PRIMARY KEY NOT NULL, 
    nombreCientifico VARCHAR(50) NOT NULL,
    nombreComun VARCHAR(50) NOT NULL,
    estadoConservacion VARCHAR(50) NOT NULL, 
    dominio VARCHAR(50) NOT NULL, 
    reino VARCHAR(50) NOT NULL, 
    filo_o_division VARCHAR(50) NOT NULL, 
    clase VARCHAR(50) NOT NULL,
    orden VARCHAR(50) NOT NULL, 
    familia VARCHAR(50) NOT NULL, 
    genero VARCHAR(50) NOT NULL,
    especie VARCHAR(50) NOT NULL,
    descripcion TEXT
);

CREATE TABLE estudio (
    idEstudio SERIAL PRIMARY KEY NOT NULL, 
    cita VARCHAR(200)
);

CREATE TABLE interaccion (
    idInteraccion SERIAL PRIMARY KEY NOT NULL, 
    idRegistro INTEGER NOT NULL,
    idEspecie INTEGER NOT NULL,
    rol VARCHAR(50)
);

CREATE TABLE registro (
    idRegistro SERIAL PRIMARY KEY NOT NULL,
    idTipoInteraccion INTEGER NOT NULL, 
    idEstudio INTEGER NOT NULL,
    idEcosistema INTEGER NOT NULL
);

CREATE TABLE tipo_interaccion (
    idTipoInteraccion SERIAL PRIMARY KEY NOT NULL, 
    nombre VARCHAR(50) NOT NULL, 
    relacion VARCHAR(10) NOT NULL
);  /* Relacion es del tipo:  "(+/-)"  por lo que varchar(10) es más que suficiente */ 


/* RELACION ENTRE TABLAS CON CLAVES FORÁNEAS */

ALTER TABLE interaccion ADD CONSTRAINT especie_fk 
FOREIGN KEY (idEspecie) REFERENCES especie (idEspecie);

ALTER TABLE interaccion ADD CONSTRAINT registro_fk 
FOREIGN KEY (idRegistro) REFERENCES registro (idRegistro);

ALTER TABLE registro ADD CONSTRAINT ecosistema_fk 
FOREIGN KEY (idEcosistema) REFERENCES ecosistema (idEcosistema);

ALTER TABLE registro ADD CONSTRAINT estudio_fk 
FOREIGN KEY (idEstudio) REFERENCES estudio (idEstudio);

ALTER TABLE registro ADD CONSTRAINT tipo_interaccion_fk 
FOREIGN KEY (idTipoInteraccion) REFERENCES tipo_interaccion (idTipoInteraccion);


/* FUNCIONES Y PROCEDIMIENTOS */

CREATE OR REPLACE FUNCTION especies_amenazadas()
RETURNS TABLE (
    idEspecie_ INTEGER,
    nombreCientifico_ VARCHAR,
    nombreComun_ VARCHAR,
    estadoConservacion_ VARCHAR
)
AS $$
BEGIN 
    RETURN QUERY 
    SELECT 
        idEspecie, 
        nombreCientifico, 
        nombreComun, 
        estadoConservacion 
    FROM especie
    WHERE estadoConservacion = 'Amenazada';
END;
$$ LANGUAGE plpgsql;
