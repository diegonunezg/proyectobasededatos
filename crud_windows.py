import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import psycopg2
from psycopg2 import Error

class CreateDialog(Gtk.Dialog):
    def __init__(self):
        Gtk.Dialog.__init__(self, title="Crear registro", parent=None, flags=0)
        self.add_button("Cancelar", Gtk.ResponseType.CANCEL)
        self.add_button("Aceptar", Gtk.ResponseType.OK)

        contenido = self.get_content_area()
        self.opcion1 = Gtk.RadioButton(label="Crear un resgitro completo (todas las tablas)")
        self.opcion2 = Gtk.RadioButton(label="Crear un resgitro solo en la tabla actual")
        self.opcion2.join_group(self.opcion1)
        contenido.add(self.opcion1)
        contenido.add(self.opcion2)

        self.set_default_size(400,100)
        self.set_modal(True)
        self.set_urgency_hint(True)
        self.show_all()
    

    def obtener_accion(self):
        if self.opcion1.get_active():
            return "all"
        else:
            return "current"


class UpdateWindow(Gtk.Dialog):
    def __init__(self, fila, atributos, tabla_actual, datos_globales_columnas=None, datos_globales_filas=None):
        Gtk.Dialog.__init__(self, title="Actualizar", parent=None, flags=0)

        self.fila = fila
        self.atributos_fila = atributos
        self.datos_globales_columnas = datos_globales_columnas
        self.datos_globales_filas = datos_globales_filas

        self.add_button("Cancelar", Gtk.ResponseType.CANCEL)
        self.add_button("Aceptar", Gtk.ResponseType.OK)

        content_box = self.get_content_area()

        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(False)
        self.grid.set_row_homogeneous(False)
        self.grid.set_column_spacing(8)
        self.grid.set_row_spacing(4)
        content_box.add(self.grid)
        
        if tabla_actual != "obtenerRegistrosInteraccion()":
            self.create_update_simples()
        else:
            self.create_update_complejos()

        self.show_all()

    
    def create_update_simples(self):
        self.entrys = []
        for i, item in enumerate(self.fila):
            self.grid.attach(Gtk.Label(label=f"{self.atributos_fila[i]}"), i%4, 2*(i//4), 1, 1)
            self.entrys.append(Gtk.Entry(text=item))
            self.grid.attach(self.entrys[-1], i%4, 2*(i//4)+1, 1, 1)
        self.grid.get_child_at(0,1).set_sensitive(False)
    

    def create_update_complejos(self):
        self.set_default_size(1200, 400)
        self.set_resizable(False)
        self.grid.attach(Gtk.Label(label="BUSCAR:"), 1, 0, 1, 1)
        self.grid.attach(Gtk.Label(label="Especie 1:"), 0, 1, 1, 1)
        self.grid.attach(Gtk.Label(label="Especie 2:"), 0, 2, 1, 1)
        self.grid.attach(Gtk.Label(label="Tipo de interacción:"), 0, 3, 1, 1)
        self.grid.attach(Gtk.Label(label="Ecosistema:"), 0, 4, 1, 1)
        self.grid.attach(Gtk.Label(label="Estudio:"), 0, 5, 1, 1)

        self.entrys_busqueda = []
        for i in range(1,6):
            entry = Gtk.Entry()
            entry.connect("changed", self.on_entry_changed)
            self.entrys_busqueda.append(entry)
            self.grid.attach(entry, 1, i, 1, 1)

        
        self.scroll = Gtk.ScrolledWindow()
        self.scroll.set_hexpand(True)
        self.scroll.set_vexpand(False)
        self.grid.attach(self.scroll, 2, 1, 7, 5)

        self.treeview = Gtk.TreeView()
        self.scroll.add(self.treeview)
        self.modelo = Gtk.ListStore(str)
        self.treeview.set_model(self.modelo)
        self.crear_columnas()
        
        self.grid.attach(Gtk.Label(label=" "), 0, 6, 1, 1)
        self.grid.attach(Gtk.Label(label="Registro a modificar:"), 4, 7, 1, 1)
        self.grid.attach(Gtk.Label(label=" "), 0, 8, 1, 1)

        self.entrys_update = []
        for i, item in enumerate(self.fila):
            self.grid.attach(Gtk.Label(label=f"{self.atributos_fila[i]}"), i, 9, 1, 1)
            self.entrys_update.append(Gtk.Entry(text=item))
            self.grid.attach(self.entrys_update[-1], i, 10, 1, 1)
            self.entrys_update[-1].set_sensitive(False)
        
        self.grid.attach(Gtk.Label(label=" "), 0, 11, 1, 1)

    
    def crear_columnas(self, tabla=None):

        for columna in self.treeview.get_columns():
            self.treeview.remove_column(columna)
        
        if tabla is None:
            tabla = ["Escribe en una de las entradas asociadas para buscar los datos que quieras modificar"]
        
        self.modelo = Gtk.ListStore(*([str]*len(tabla)))
        self.treeview.set_model(self.modelo)

        for i, nombre_columna in enumerate(tabla):
            celda = Gtk.CellRendererText()
            columna = Gtk.TreeViewColumn(nombre_columna, celda, text=i)
            self.treeview.append_column(columna)


    def cambiar_tabla(self, widget, query):

        self.tabla_actual = query

        self.treeview.get_model().clear()

        self.cursor.execute(f"SELECT * FROM {query};")

        column_names = [desc[0] for desc in self.cursor.description]

        self.crear_columnas(column_names)

        data_rows = []
        for row in self.cursor:
            data_rows.append(row)
        
        for item in data_rows:
            line = [str(x) for x in item]
            self.modelo.append(line)



    def on_entry_changed(self, entry):
        self.connect_to_postgres()

        if self.entrys_busqueda.index(entry) == 0 or self.entrys_busqueda.index(entry) == 1:
            self.cambiar_tabla(entry, f"buscarTextoEnEspecie('{entry.get_text()}')")

        self.cursor.close()
        self.connection.close()

    
    def get_cambios(self):
        return [entry.get_text() for entry in self.entrys_update]

    
    def connect_to_postgres(self):

        try:
            self.connection = psycopg2.connect(
                user = "postgres",
                password = "delta733",
                host = "localhost",
                port = 5432,
                database = "relaciones_especies"
            )

            self.cursor = self.connection.cursor()

        except (Exception, Error) as error:
            print("Error en PostgreSQL", error)
            Gtk.main_quit()



class CreateWindow(Gtk.Dialog):
    def __init__(self, cols_especie, data_especie):
        Gtk.Dialog.__init__(self, title="Crear registro", parent=None, flags=0)
        self.add_button("Cancelar", Gtk.ResponseType.CANCEL)
        self.add_button("Aceptar", Gtk.ResponseType.OK)

        content_box = self.get_content_area()
        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(False)
        self.grid.set_row_homogeneous(False)
        self.grid.set_column_spacing(8)
        self.grid.set_row_spacing(4)
        content_box.add(self.grid)

        self.set_default_size(1200, 800)
        self.set_resizable(False)
        self.set_title("Crear")
        self.set_border_width(8)

        self.columnas = []

        self.cols_especie = cols_especie
        self.data_especie = data_especie

        self.column_especie()

        self.show_all()


    def column_especie(self):

        # Un registro involucra a 2 especies, por lo que hay que añadir 2 columnas para datos de especies
        for esp in range(2):
            # La columna contiene todos los entry para añadir los datos asociados a cada especie
            column = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            column.pack_start(Gtk.Label(label=f"Especie {esp+1}:"), False, True, 0)
            column.pack_start(Gtk.Label(label=" "), False, True, 0)

            # Esto crea un label y un entry por cada atributo de la entidad especie
            for i, col in enumerate(self.cols_especie):
                comboboxtext_i = Gtk.ComboBoxText.new_with_entry()
                comboboxtext_i.set_entry_text_column(0)
                entry_comboboxtext_i = comboboxtext_i.get_child()
                entry_comboboxtext_i.connect("changed", self.on_entry_esp_changed, comboboxtext_i, i)
                entry_comboboxtext_i.set_activates_default(True)
                if i == 0:
                    entry_comboboxtext_i.set_sensitive(False)

                entry_completion_i = Gtk.EntryCompletion()
                entry_comboboxtext_i.set_completion(entry_completion_i)
                entry_completion_i.set_model(Gtk.ListStore(str))
                entry_completion_i.set_text_column(0)

                for row in self.data_especie:
                    comboboxtext_i.append_text(row[i])
                
                column.pack_start(Gtk.Label(label=f"{self.cols_especie[i]}"), False, True, 0)
                column.pack_start(comboboxtext_i, False, True, 0)
            
            entry_rol_esp = Gtk.Entry()
            entry_rol_esp.set_activates_default(True)

            column.pack_start(Gtk.Label(label=f"Rol de la especie {esp}"), False, True, 0)
            column.pack_start(entry_rol_esp, False, True, 0)

            self.grid.attach(column, esp, 0, 1, 1)
    
        
    def on_entry_esp_changed(self, entry, comboboxtext, index):
        texto_busqueda = entry.get_text().strip().lower()

        comboboxtext.set_active(-1)
        comboboxtext.remove_all()
        
        datos = []
        for row in self.data_especie:
            datos.append(str(row[index]))
        
        datos = list(set(datos))  # Set elimina los duplicados y solo deja las palabras únicas

        for dato in datos:
            if texto_busqueda in dato.lower():
                comboboxtext.append_text(dato)
        












































"""
    def column_especieasd(self):

        column1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        column1.pack_start(Gtk.Label(label="Especie 1:"), False, True, 0)

        self.cbt_e1_nombrecientifico = Gtk.ComboBoxText.new_with_entry()
        self.cbt_e1_nombrecientifico.set_entry_text_column(0)

        entry_e1_nombrecientifico = self.cbt_e1_nombrecientifico.get_child()
        entry_e1_nombrecientifico.connect("changed", self.on_entry_changed)
        entry_e1_nombrecientifico.set_activates_default(True)

        self.entry_completion_especie1 = Gtk.EntryCompletion()
        entry_e1_nombrecientifico.set_completion(self.entry_completion_especie1)
        self.entry_completion_especie1.set_model(Gtk.ListStore(str))
        self.entry_completion_especie1.set_text_column(0)
        self.entry_completion_especie1.set_match_func(self.match_func, None)

        for row in self.data_especie:
            self.cbt_e1_nombrecientifico.append_text(row[1])

        column1.pack_start(self.cbt_e1_nombrecientifico, False, True, 0)
        
        self.grid.attach(column1, 0, 0, 1, 1)


        column2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        column2.pack_start(Gtk.Label(label="Especie 2:"), False, True, 0)

        self.cbt_e2_nombrecientifico = Gtk.ComboBoxText.new_with_entry()
        self.cbt_e2_nombrecientifico.set_entry_text_column(0)

        entry_e2_nombrecientifico = self.cbt_e2_nombrecientifico.get_child()
        entry_e2_nombrecientifico.connect("changed", self.on_entry_changed)
        entry_e2_nombrecientifico.set_activates_default(True)

        self.entry_completion_especie2 = Gtk.EntryCompletion()
        entry_e2_nombrecientifico.set_completion(self.entry_completion_especie2)
        self.entry_completion_especie2.set_model(Gtk.ListStore(str))
        self.entry_completion_especie2.set_text_column(0)
        self.entry_completion_especie2.set_match_func(self.match_func, None)

        for row in self.data_especie:
            self.cbt_e2_nombrecientifico.append_text(row[1])

        column2.pack_start(self.cbt_e2_nombrecientifico, False, True, 0)
        
        self.grid.attach(column2, 1, 0, 1, 1)
"""

